const express = require("express")

const PORT = (() => {
  if (!process.env.PORT) return 56201
  const portNum = Number.parseInt(process.env.PORT, 10)
  if (Number.isNaN(portNum)) {
    console.error(
      `Expected PORT env variable to be an integer, got ${process.env.PORT}`
    )
    process.exit(-1)
  }
  return portNum
})()

// Oh my god! I hate how JS date weeks starts from
// sunday! God damn it
const WEEKS = [
  "Sunday",
  "Monday",
  "Tuesday",
  "Wednesday",
  "Thursday",
  "Friday",
  "Saturday",
]

const MILLISECONDS_IN_DAY = 60 * 60 * 24 * 1000

express()
  .post("/square", async (req, res) => {
    const inputStr = await expectContentOfType(req, "text/plain")
    if (!inputStr) {
      return sendInvalidMimeType(res, "text/plain", req.headers["content-type"])
    }
    const input = Number.parseFloat(inputStr)
    if (Number.isNaN(input)) {
      return sendInvalidRequestError(res)
    }

    res.status(200).json({ number: input, square: input * input })
  })
  .post("/reverse", async (req, res) => {
    const input = await expectContentOfType(req, "text/plain")
    if (!input) {
      return sendInvalidMimeType(res, "text/plain", req.headers["content-type"])
    }
    res.contentType("text/plain").status(200).end(reverseString(input))
  })
  .get("/date/:year/:month/:day", (req, res) => {
    const { year, month, day } = req.params
    const date = new Date(
      Number.parseInt(year, 10),
      Number.parseInt(month, 10) - 1,
      Number.parseInt(day, 10)
    )
    if (Number.isNaN(date.valueOf())) {
      return sendInvalidRequestError(res)
    }

    res.status(200).json(dateResponse(date))
  })
  .listen(PORT, () => {
    console.info(`Started application on port ${PORT}`)
  })

async function collectStreamToString(stream, encoding) {
  let buffer = ""
  for await (const chunk of stream) {
    buffer += chunk.toString(encoding)
  }
  return buffer
}

function reverseString(input) {
  return Array.from(input).reverse().join("")
}

function today() {
  const now = new Date()
  return new Date(now.getFullYear(), now.getMonth(), now.getDate())
}

function differenceInDays(to, from) {
  // Date timestamp in milliseconds does not fit nicely in JS's 53 bit integers
  // Since we are dealing with days at least, it is fine to loose precision on
  // least significant digits. Day is 8,64 * 10^7 milliseconds so we win quite
  // a few digits in the end
  return Number((BigInt(to) - BigInt(from)) / BigInt(MILLISECONDS_IN_DAY))
}

// https://en.wikipedia.org/wiki/Leap_year#Algorithm
function isLeapYear(year) {
  if (year % 4 !== 0) return false
  else if (year % 100 !== 0) return true
  else if (year % 400 !== 0) return false
  else return true
}

function extractCharset(components) {
  for (const component of components) {
    const [key, value] = component.split("=")
    if (key === "charset") {
      return value
    }
  }
}

function parseContentType(header) {
  const [contentType, ...components] = header.split(/; */)
  const charset = extractCharset(components)
  return { contentType, charset }
}

function matchesContentType(
  req,
  requiredContentType,
  charsets = ["ascii", "utf-8"]
) {
  const header = req.headers["content-type"]
  if (!header) return { matches: false }
  const { contentType, charset } = parseContentType(header)
  const matches =
    contentType === requiredContentType &&
    (!charset || charsets.includes(charset))
  return { matches, charset }
}

function expectContentOfType(req, expectedContentType) {
  const { matches, charset } = matchesContentType(req, expectedContentType)
  if (!matches) return
  return collectStreamToString(req, charset)
}

function sendInvalidRequestError(res) {
  res.status(400).json({ error: "Invalid input" })
}

function sendInvalidMimeType(res, expected, got) {
  res
    .status(400)
    .json({ error: `Invalid MIME type of ${got}, expected ${expected}` })
}

function dateResponse(date) {
  return {
    weekDay: WEEKS[date.getDay()],
    difference: Math.abs(differenceInDays(date, today())),
    isLeapYear: isLeapYear(date.getFullYear()),
  }
}
